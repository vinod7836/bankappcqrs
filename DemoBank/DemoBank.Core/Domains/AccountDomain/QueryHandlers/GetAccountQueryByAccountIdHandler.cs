﻿using CQRS.BankApp.Core.CQRS;
using CQRS.BankApp.Core.Domains.AccountDomain.Queries;
using CQRS.BankApp.Persistance.Entities;
using CQRS.BankApp.Persistance.Repositories;

namespace CQRS.BankApp.Core.Domains.AccountDomain.QueryHandlers
{
    public class GetAccountQueryByAccountIdHandler : IHandleQuery<GetAccountQueryById, BankAccounts>
    {
        private GenericRepository<BankAccounts> _accounts;

        public GetAccountQueryByAccountIdHandler(GenericRepository<BankAccounts> accounts)
        {
            _accounts = accounts;
        }
        public BankAccounts Handle(GetAccountQueryById query)
        {
            return _accounts.GetById(query.AccountId);
        }
    }
}
