﻿using CQRS.BankApp.Core.CQRS;
using CQRS.BankApp.Core.Domains.AccountDomain.Queries;
using CQRS.BankApp.Core.Models;
using CQRS.BankApp.Persistance.Entities;
using CQRS.BankApp.Persistance.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace CQRS.BankApp.Core.Domains.AccountDomain.QueryHandlers
{
    public class GetAccountsForUserIdQueryHandler : IHandleQuery<GetAccountForUserIdQuery, IEnumerable<BankAccountModel>>
    {
        private readonly GenericRepository<BankAccounts> _bankAccountRepository;

        public GetAccountsForUserIdQueryHandler(GenericRepository<BankAccounts> bankAccountRepository)
        {
            _bankAccountRepository = bankAccountRepository;
        }
        public IEnumerable<BankAccountModel> Handle(GetAccountForUserIdQuery query)
        {
            return _bankAccountRepository.GetAll().GroupBy(a => new { a.Id, a.Balance, a.AccountNo, a.Login }).Where(a => a.Key.Login.Id == query.UserId).Select(x => new BankAccountModel
            {
                AccountNo = x.Key.AccountNo,
                Balance = x.Key.Balance,
                Id = x.Key.Id
            });
        }
    }
}
