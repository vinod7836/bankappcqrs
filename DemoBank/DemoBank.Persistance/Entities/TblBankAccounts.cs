﻿namespace CQRS.BankApp.Persistance.Entities
{
    public class BankAccounts : IMockEntity
    {
        public int Id { get; set; }
        public string AccountNo { get; set; }
        public decimal Balance { get; set; }
        public UserLogins Login { get; set; }
    }
}
