﻿using System.Collections.Generic;

namespace CQRS.BankApp.Persistance.Entities
{
    public class UserLogins : IMockEntity
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password => "demo";
        public virtual List<BankAccounts> BankAccounts { get; set; }
        public virtual List<BankAccounts> PreDefinedAccounts { get; set; }
    }
}
